defmodule AloyUtilsEnvTest do
  use ExUnit.Case, async: true
  doctest AloyUtils.Env

  setup %{tmp_dir: dir} do
    current_dir = File.cwd!()

    File.cd!(dir)
    File.write!(".env", "__AU_E_TEST_VAR=test")

    on_exit(fn ->
      File.cd!(current_dir)
    end)
  end

  @tag :tmp_dir
  test "dotenv/1 load" do
    AloyUtils.Env.dotenv()
    assert System.fetch_env!("__AU_E_TEST_VAR") == "test"
  end

  @tag :tmp_dir
  test "dotenv_dev/2 load_dev is dev" do
    AloyUtils.Env.dotenv_dev(true)
    assert System.fetch_env!("__AU_E_TEST_VAR") == "test"
  end

  @tag :tmp_dir
  test "dotenv_dev/2 load_dev is not dev" do
    AloyUtils.Env.dotenv_dev(false)
    assert System.fetch_env("__AU_E_TEST_VAR") == :error
  end
end
