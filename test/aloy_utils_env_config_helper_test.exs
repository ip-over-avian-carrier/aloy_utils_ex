defmodule AloyUtilsEnvConfigHelperTest do
  use ExUnit.Case, async: true
  doctest AloyUtils.Env.ConfigHelpers

  alias AloyUtils.Env.ConfigHelpers

  describe "ConfigHelpers.get_env/3" do
    setup do
      System.put_env("__AU_E_CH_TEST_STRING", "test")
      System.put_env("__AU_E_CH_TEST_INTEGER", "420")
      System.put_env("__AU_E_CH_TEST_BOOL_T", "true")
      System.put_env("__AU_E_CH_TEST_BOOL_F", "false")
      System.put_env("__AU_E_CH_TEST_JSON", "[1, 2, 3]")
      System.put_env("__AU_E_CH_TEST_UNKN", "value unknown")

      :ok
    end

    test "default args exists" do
      assert ConfigHelpers.get_env("__AU_E_CH_TEST_STRING") == "test"
      assert ConfigHelpers.get_env("__AU_E_CH_TEST_INTEGER") == "420"
    end

    test "default args does not exist" do
      assert_raise System.EnvError, fn ->
        ConfigHelpers.get_env("__AU_E_CH_TEST_MISSING")
      end
    end

    test "string" do
      assert ConfigHelpers.get_env("__AU_E_CH_TEST_STRING", :no_default, :string) == "test"
    end

    test "integer" do
      assert ConfigHelpers.get_env("__AU_E_CH_TEST_INTEGER", :no_default, :integer) == 420
    end

    test "bool true" do
      assert ConfigHelpers.get_env("__AU_E_CH_TEST_BOOL_T", :no_default, :boolean)
    end

    test "bool false" do
      assert not ConfigHelpers.get_env("__AU_E_CH_TEST_BOOL_F", :no_default, :boolean)
    end

    test "json" do
      assert_raise RuntimeError, "Please include Jason if you intend to load json", fn ->
        ConfigHelpers.get_env("__AU_E_CH_TEST_JSON", :no_default, :json)
      end
    end

    test "invalid type" do
      assert_raise RuntimeError, "Cannot convert to :binary: \"value unknown\"", fn ->
        ConfigHelpers.get_env("__AU_E_CH_TEST_UNKN", :no_default, :binary)
      end
    end

    test "does not exist raise" do
      assert_raise System.EnvError, fn ->
        ConfigHelpers.get_env("__AU_E_CH_TEST_MISSING", :no_default, :string)
      end
    end

    test "does not exist default" do
      assert ConfigHelpers.get_env("__AU_E_CH_TEST_MISSING", "default_value", :string) ==
               "default_value"
    end

    test "exists default" do
      assert ConfigHelpers.get_env("__AU_E_CH_TEST_STRING", "default_value", :string) == "test"
    end
  end
end
