defmodule AloyUtils.Env do
  @moduledoc """
  Utilities to do with environment variables
  """

  @doc """
  Load the env file specified into the environment.

  This is short for DotenvParser.load_file(path)
  """
  @spec dotenv(String.t()) :: :ok
  def dotenv(path \\ ".env") when is_binary(path) do
    DotenvParser.load_file(path)
  end

  @doc """
  Shorthand for only calling dotenv/1 if the given condition is true
  """
  @spec dotenv_dev(boolean(), String.t()) :: :ok
  def dotenv_dev(is_dev, path \\ ".env") when is_boolean(is_dev) do
    if is_dev do
      dotenv(path)
    else
      :ok
    end
  end
end
