defmodule AloyUtils.Scm do
  @moduledoc """
  Utilities for extracting information from a scm
  """

  def atoi(s) do
    {i, ""} = String.trim(s) |> Integer.parse()
    i
  end

  @doc """
  Retrieve the status of the currently checked out (or given) fossil repo as a map.
  """
  def fossil_status(r \\ nil) do
    args = (if r != nil, do: ["-R", r], else: []) ++ ~w(json status)
    System.cmd("fossil", args)
    |> Jason.decode!()
  end

  @doc """
  Retrieve a simple info about the current checkout/commit from the given scm
  """
  @spec simple(:fossil | :git) :: [commit: String.t(), branch: String.t(), timestamp: DateTime.t()]
  def simple(scm)

  def simple(:fossil) do
    i = fossil_status()
    [
      commit: i["payload"]["checkout"]["uuid"],
      branch: i["payload"]["checkout"]["tags"] |> List.first(),
      timestamp: DateTime.from_unix!(i["payload"]["checkout"]["timestamp"])
    ]
  end

  def simple(:git) do
    [
      commit: System.cmd("git", ~w(rev-parse HEAD)),
      branch: System.cmd("git", ~w(branch --show-current)),
      timestamp: System.cmd("git", ~w(log -n1 --pretty="%at")) |> atoi() |> DateTime.from_unix!()
    ]
  end
end
