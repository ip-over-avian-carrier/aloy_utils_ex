defmodule AloyUtils.Env.ConfigHelpers do
  @type config_type :: :string | :integer | :boolean | :json

  @doc """
  Get value from environment variable, converting it to the given type if needed.

  If no default value is given, or `:no_default` is given as the default, an error is raised if the variable is not
  set.
  """
  @spec get_env(String.t(), :no_default | any(), config_type()) :: any()
  def get_env(var, default \\ :no_default, type \\ :string)

  def get_env(var, :no_default, type) do
    System.fetch_env!(var)
    |> get_with_type(type)
  end

  def get_env(var, default, type) do
    with {:ok, val} <- System.fetch_env(var) do
      get_with_type(val, type)
    else
      :error -> default
    end
  end

  @doc """
  Same as `get_env/3`, but it checks first if a `\#{var}_FILE` variable is defined.

  If such a var is found it instead attempts to read the value from the referenced file.
  """
  @spec get_env(String.t(), :no_default | any(), config_type()) :: any()
  def get_env_file(var, default \\ :no_default, type \\ :string) do
    file_env = get_env("#{var}_FILE", nil)

    if file_env != nil do
      File.read!(file_env)
      |> get_with_type(type)
    else
      get_env(var, default, type)
    end
  end

  @spec get_with_type(String.t(), config_type()) :: any()
  defp get_with_type(val, type)

  defp get_with_type(val, :string), do: val
  defp get_with_type(val, :integer), do: String.to_integer(val)
  defp get_with_type("true", :boolean), do: true
  defp get_with_type("false", :boolean), do: false

  if Code.ensure_loaded?(Jason) do
    defp get_with_type(val, :json), do: Jason.decode!(val)
  else
    defp get_with_type(_jason_not_found, :json),
      do: raise("Please include Jason if you intend to load json")
  end

  defp get_with_type(val, type), do: raise("Cannot convert to #{inspect(type)}: #{inspect(val)}")
end
