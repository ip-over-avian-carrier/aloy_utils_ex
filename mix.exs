defmodule AloyUtils.MixProject do
  use Mix.Project

  def project do
    [
      app: :aloy_utils,
      version: "0.1.2",
      elixir: "~> 1.17",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dotenv_parser, "~> 2.0.1"},
      {:junit_formatter, "~> 3.4", only: [:test]},
      {:jason, "~> 1.4.4"}
    ]
  end
end
